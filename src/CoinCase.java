
class CoinCase {

	private int type500 =0;
	private int type100 =0;
	private int type50 =0;
	private int type10 =0;
	private int type5 =0;
	private int type1 =0;


	public void AddCoins(int type,int count) {
		switch(type) {
		case 500:
			type500 += count;
			break;
		case 100:
			type100 += count;
			break;
		case 50:
			type50 += count;
			break;
		case 10:
			type10 += count;
			break;
		case 5:
			type5 += count;
			break;
		case 1:
			type1 += count;
			break;
		}
	}
	public int GetCount(int type) {
		switch(type) {
		case 500:
			return type500;
		case 100:
			return type100;
		case 50:
			return type50;
		case 10:
			return type10;
		case 5:
			return type5;
		case 1:
			return type1;
		}
		return 0;
	}
	public int GetAmount() {
		return  ( type500 * 500 ) + ( type100 * 100 ) + ( type50 * 50 ) + ( type10 * 10 ) + ( type5 * 5 ) + type1;
	}

}
