import java.util.Scanner;

class Main {


	public static void main( String[] args ){
		Scanner scanner = new Scanner(System.in);
		CoinCase coinCase = new CoinCase();


		for(int i = 1; i <= 10; i++) {

			System.out.print("硬貨の種類：");
			int type = scanner.nextInt();
			System.out.print("硬貨の枚数：");
			int count = scanner.nextInt();

			coinCase.AddCoins(type, count);

		}
		scanner.close();

		System.out.println("500円" + coinCase.GetCount(500) + "枚です。");
		System.out.println("100円" + coinCase.GetCount(100) + "枚です。");
		System.out.println("50円" + coinCase.GetCount(50) + "枚です。");
		System.out.println("10円" + coinCase.GetCount(10) + "枚です。");
		System.out.println("5円" + coinCase.GetCount(5) + "枚です。");
		System.out.println("1円" + coinCase.GetCount(1) + "枚です。");
		System.out.println("総額は" + coinCase.GetAmount() + "円です。" );
	}

}